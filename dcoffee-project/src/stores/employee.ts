import type { Employee } from '@/types/Employee'
import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useEmployee = defineStore('employee', () => {
  const employees = ref<Employee[]>([
    {
      id: 1,
      name: 'โนบิตะ โนบิ',
      positon: 'เจ้าของร้าน',
      salary: 15000
    },
    {
      id: 2,
      name: 'มิซึริ คันโรจิ',
      positon: 'ผู้จัดการร้าน',
      salary: 10000
    },
    {
      id: 3,
      name: 'ชินอีจิ คุโด้',
      positon: 'พนักงานเสิร์ฟ',
      salary: 8000
    },
    {
      id: 4,
      name: 'ทาเคชิ โกดะ',
      positon: 'พนักงานเสิร์ฟ',
      salary: 8000
    },
    {
      id: 5,
      name: 'ไจโกะ โกดะ',
      positon: 'พนักงานแคชเชียร์',
      salary: 8000
    },
    {
      id: 6,
      name: 'โนบิซุเกะ โนบิ',
      positon: 'พนักงานชงเครื่องดื่ม',
      salary: 8000
    },
    {
      id: 7,
      name: 'ชิซุกะ มินาโมโตะ',
      positon: 'พนักงานทำครัว',
      salary: 8000
    },
    {
      id: 8,
      name: 'เดคิซุงิ เดไซ',
      positon: 'พนักงานทำครัว',
      salary: 8000
    },
    {
      id: 9,
      name: 'ซึเนโอะ โฮเนคาว่า',
      positon: 'พนักงานล้างจาน',
      salary: 6000
    },
    {
      id: 10,
      name: 'เซวาชิ โนบิ',
      positon: 'พนักงานล้างจาน',
      salary: 6000
    }
  ])

  return {
    employees
  }
})
