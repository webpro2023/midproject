import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    { id: 1, name: 'อานี่ เธียร์รี่', tel: '0812345678' },
    { id: 2, name: 'โยอิจิ อิซากิ', tel: '0823456789' },
    { id: 3, name: 'เรสเค คิระ', tel: '0834567891' },
    { id: 4, name: 'เมกุรุ บาชิระ', tel: '0845678912' },
    { id: 5, name: 'เร็นสุเกะ คุนิกามิ', tel: '0856789123' },
    { id: 6, name: 'เหมะ ชิกิริ', tel: '0867891234' },
    { id: 7, name: 'จิงโกะ ไรจิ', tel: '0878912345' },
    { id: 8, name: 'วาตารุ คุน', tel: '0889123456' },
    { id: 9, name: 'จิน กากามารุ', tel: '0891234567' },
    { id: 10, name: 'ยูได อิมามูระ', tel: '0898765432' }
  ])
  const currentMember = ref<Member | null>()
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }
  function clear() {
    currentMember.value = null
  }
  return {
    members,
    currentMember,
    searchMember,
    clear
  }
})
