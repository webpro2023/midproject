import type { Dessert } from './Dessert'

type DessertItem = {
  id: number
  name: string
  position: string
  checkIn: boolean
  checkOut: boolean
  createdate: Date
  checkInTime?: Date
  checkOutTime?: Date
  dpessert?: Dessert
  // round: number;
}

export { type DessertItem }
