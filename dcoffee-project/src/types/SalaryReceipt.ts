type SalaryReceipt = {
  id: number
  date: string
  totalSalary: number
  paymentStatus: boolean
}

export { type SalaryReceipt }
